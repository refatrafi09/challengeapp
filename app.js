/*
 * This file is generated and updated by Sencha Cmd. You can edit this file as
 * needed for your application, but these edits will have to be merged by
 * Sencha Cmd when upgrading.
 */
Ext.application({
    name: 'ChallengeApp',

    extend: 'ChallengeApp.Application',

    requires: [
        'ChallengeApp.view.main.UserGrid',
        'ChallengeApp.model.Users',
        'ChallengeApp.model.Activities',
        'ChallengeApp.view.main.ActivityGrid'
    ],

    // The name of the initial view to create. With the classic toolkit this class
    // will gain a "viewport" plugin if it does not extend Ext.Viewport. With the
    // modern toolkit, the main view will be added to the Viewport.
    //
    // mainView: 'ChallengeApp.view.main.UserGrid', 
    mainView: 'ChallengeApp.view.main.ActivityGrid',
    //-------------------------------------------------------------------------
    // Most customizations should be made to ChallengeApp.Application. If you need to
    // customize this file, doing so below this section reduces the likelihood
    // of merge conflicts when upgrading to new versions of Sencha Cmd.
    //-------------------------------------------------------------------------
    launch: function () {
        Ext.require('CsvReader');
        Ext.create('Ext.container.Viewport', {
            layout: 'border',
            items: [
                {
                    region: 'center',
                    xtype: 'ActivityGrid',
                }]
        });
    }
});
