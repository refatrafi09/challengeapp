# ChallengeApp


The learning with Ext JS was so amazing. Thanks for giving me the opportunity. 
Although it was very  short time to study with a new Thing, as I am a full time employee here in Bangladesh. 
Anyways i have divided my thoughts after a short tour on two parts. 

- Project Details
- Challenges I have faced


## Project 

As, I was using Sencha Cmd, So decided to code only on `Sencha classic` for having a tight schedule. `MVVM` pattern is strictly followrd. Following is a short description of the important files.

## Model
- `Users.js` -> Model coded for using users.cvs data to show on the Grid
- `Activities.js` -> Model coded for using activities.csv data to show on Grid

## View
- `ActivityGrid.js` -> View for the Activity Grid
- `UserGrid.js` -> View for the User Grid

## ViewModel
- `allViewModel.js` -> viewmodel for the UserGrid.js and ActivityGrid.js view files.

## HelperLibray 
- `csvReader.js` -> Extened the Ext.data.reader api to implement the csv reader and turn the data into a JSON. path of the file is view/main/helperLibrary/csvReader.js

Here in the Bitbucket there is two branches. 

- **master** -> Has the code till the last step. Using activities.csv and build a grid.
- **rafi/step3** -> has the code till step-3 The work upon writing a CSV Reader. used users.csv to build a Grid


## Challenges I have faced

* The first challenge was to understand the `MVVM` and implement it in the Little App. The links you have provided helped me a lot.
* Then all of my focus is on the csv reader. I have used the Ext.data.reader.json api. At first had no idea about implementing it. The source code of http://docs.sencha.com/extjs/6.0.1/classic/src/Json.js.html gave me a full idea of implementing.




