Ext.define('ChallengeApp.view.main.UserGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.UserGrid',
    config: {
    },
    viewModel: { type: 'allViewModel' },
    constructor: function (config) {
        return this.callParent(arguments);
    },
    selType: 'cellmodel', // selects a cell of the table. Options: rowmodel
    columnLines: true,
    selModel:
    {
        mode: 'SINGLE'
    },
    viewConfig:
    {
        stripeRows: true // Grid view level configuration
    },
    bind: {
        store: '{UsersData}' // Data binding with the help of viewModel
    },
    initComponent: function () {
        Ext.apply(this, {
            columns: [{
                xtype: "rownumberer"
            },
            {
                text: "Name",
                flex: 1,
                dataIndex: "Name" // Key of the data Object of the Array of Objects
            },  {
                text: "First Name",
                flex: 1,
                dataIndex: "Firstname"
            },  {
                text: "Last Name",
                flex: 1,
                dataIndex: "Lastname"
            },  {
                text: "Department",
                flex: 1,
                dataIndex: "Department"
            },  {
                text: "Email",
                flex: 1,
                dataIndex: "E-Mail *"
            },  {
                text: "Login",
                flex: 1,
                dataIndex: "Login"
            }, {
                text: "Status",
                flex: 1,
                dataIndex: "Status"
            }]
        });

        this.callParent(arguments);
    }
});