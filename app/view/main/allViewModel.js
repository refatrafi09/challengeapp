Ext.define('ChallengeApp.view.main.UserViewModel', {
    extend: 'Ext.app.ViewModel',
    alias:'viewmodel.allViewModel',
    requires:[
        'ChallengeApp.model.Users',
        'ChallengeApp.model.Activities'
    ],
    stores: {
        UsersData: {
            model: 'ChallengeApp.model.Users',
            autoLoad: true,
        },
        ActivityData: {
            model: 'ChallengeApp.model.Activities',
            autoLoad: true,
        }
    }
});