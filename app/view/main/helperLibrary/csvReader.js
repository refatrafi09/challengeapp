Ext.define('CsvReader', {
    extend: 'Ext.data.reader.Json',
    alias : 'reader.csv',

    convertoJson: function(csvData){
      var lines = csvData.split("\n"); // convert the string into array based on new line
      if (lines.length) {
          var itteration = 0;
          var colNames = null;
          while(itteration < lines.length) { // find out the row which has the column.
            if (lines[itteration] === "") {
                itteration++;
                continue;
            }
            colNames = lines[itteration].split(";");

            // Trim space between key names
            for(var i = 0; i< colNames.length; i++) {
              if (colNames[i].indexOf(' ') >= 0) {
                colWords = colNames[i].split(" ");
                let newColWord = '';
                colWords.map(function(word){
                  newColWord += word;
                });
                colNames[i]=newColWord;
              }
            }

            break;
          }
          var records = [];
          for(var i = 1; i < lines.length; i++) { // process the data rows
            if (lines[i] === "") continue;
            var record = {};
            var cells = lines[i].split(";");
            for (var j = 0; j < cells.length; j++) {
              record[colNames[j]] = cells[j];
            }
            records.push(record);
          }
      }
      return records;
    },

    // override
    getResponseData: function(response) {
        try {
            return this.processRecords(response.responseText);
        } catch (ex) {
            error = new Ext.data.ResultSet({
                total  : 0,
                count  : 0,
                records: [],
                success: false,
                message: ex.message
            });
            this.fireEvent('exception', this, response, error);
            console.log(error);
            return error;
        }
    },

    // override
    processRecords: function(rawData) {
        var proccessedData = this.convertoJson(rawData);
        return proccessedData;
    }
});
