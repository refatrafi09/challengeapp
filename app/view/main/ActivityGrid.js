Ext.define('ChallengeApp.view.main.ActivityGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.ActivityGrid',
    config: {
    },
    viewModel: { type: 'allViewModel' },
    constructor: function (config) {
        return this.callParent(arguments);
    },
    selType: 'cellmodel', // selects a cell of the table. Options: rowmodel
    columnLines: true,
    selModel:
    {
        mode: 'SINGLE'
    },
    viewConfig:
    {
        stripeRows: true // Grid view level configuration
    },
    bind: {
        store: '{ActivityData}' // Data binding with the help of viewModel
    },
    initComponent: function () {
        Ext.apply(this, {
            columns: [{
                xtype: "rownumberer"
            },
            {
                text: "Activity",
                flex: 1,
                dataIndex: "Activity"
            },  {
                text: "Plan Start",
                flex: 1,
                dataIndex: "PlannedStart"
            },  {
                text: "Plan End",
                flex: 1,
                dataIndex: "PlannedEnd"
            },  {
                text: "Actual Start",
                flex: 1,
                dataIndex: "ActualStart"
            },  {
                text: "Actual End",
                flex: 1,
                dataIndex: "ActualEnd"
            }]
        });

        this.callParent(arguments);
    }
});