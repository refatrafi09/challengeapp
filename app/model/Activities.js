Ext.define('ChallengeApp.model.Activities', {
    extend: 'Ext.data.Model',
    schema: {
        id: 'ActivitySchema',
        namespace: 'ChallengeApp.model',
        proxy: {
            type: 'ajax',
            url: 'app/model/activities.csv',
            reader: 'csv'
        }
    },
    fields: [
        { name: 'Activity', type: 'string' },
        { name: 'PlannedStart', type: 'string' },
        { name: 'ActualStart', type: 'string' },
        { name: 'PlannedEnd', type: 'string' },
        { name: 'ActualEnd', type: 'string' },
        { name: 'Responsibleuser', type: 'string' }
    ]
});