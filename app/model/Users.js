Ext.define('ChallengeApp.model.Users', {
    extend: 'Ext.data.Model',
    schema: {
        id: 'userSchema',
        namespace: 'ChallengeApp.model',
        proxy: {
            type: 'ajax',
            url: 'app/model/users.csv',
            reader: 'csv'
        }
    },
    fields: [
        { name: 'Name', type: 'string' },
        { name: 'Firstname', type: 'string' },
        { name: 'Lastname', type: 'string' },
        { name: 'Department', type: 'string' },
        { name: 'E-Mail *', type: 'string' },
        { name: 'Login', type: 'string' },
        { name: 'Status', type: 'string' }
    ]
});